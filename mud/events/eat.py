from .event import Event2

class EatEvent(Event2):
    NAME = "eat"

    def perform(self):
        if not self.object.has_prop("eatable"):
            self.add_prop("object-not-eatable")
            return self.eat_failed()
        self.inform("eat")

    def eat_failed(self):
        self.fail()
        self.inform("eat.failed")
