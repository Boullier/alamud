from .event import Event1

class ListenEvent(Event1):
    NAME = "listen"

    def get_event_templates(self):
        return self.actor.container().get_event_templates()

    def perform(self):
        self.buffer_clear()
        self.buffer_inform("listen.actor", object=self.actor.container())
        players = []
        objects = []
        for x in self.actor.container().contents():
            if x is self.actor:
                pass
            elif x.is_player():
                players.append(x)
            else:
                objects.append(x)
        self.actor.send_result(self.buffer_get())
